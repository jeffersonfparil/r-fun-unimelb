# R-fun-unimelb

## Having fun in R

Hi guys,
\
\
This will be the repository of all the codes we will generate. You can visit this site any time to see the progress and I will be updating the stuff here regularly. The R notebooks are found in [src/pdf/ folder](https://gitlab.com/jeffersonfparil/r-fun-unimelb/tree/master/src/pdf). The R codes are in the [src/ folder](https://gitlab.com/jeffersonfparil/r-fun-unimelb/tree/master/src). The sample data are in the [res/ folder](https://gitlab.com/jeffersonfparil/r-fun-unimelb/tree/master/res). Play around with everything in this repository, don't worry about messing things up, the files here won't be affected.
\
\
You can download R if you have time. You may use the links I have provided here. I recommend installing Rstudio after installing R as it will greatly help in building interactive notebooks which can be helpful in tracking and publishing codes and graphs.


- Download R: [https://cran.ms.unimelb.edu.au/](https://cran.ms.unimelb.edu.au/)
- Optional but recommended:
    + RStudio (nice GUI): [https://www.rstudio.com/products/rstudio/download/#download](https://www.rstudio.com/products/rstudio/download/#download)
        * Installation is straightforward in Windows and Mac
        * In ubuntu:
            - `sudo apt install r-base`
            - `sudo apt install gdebi-core`
            - [Download Rstudio for ubuntu](https://download1.rstudio.org/rstudio-xenial-1.1.463-amd64.deb)
            - `sudo gdebi ~/Downloads/rstudio*.deb`
        * May need to install knitr for R notebooks publication: `install.packages('knitr', repos = c('http://rforge.net',`
          `'http://cran.rstudio.org'), type = 'source')` 

<!--     + Anaconda (the whole sci-computing shebang!):
        - For Windows: [https://www.anaconda.com/download/#download](https://www.anaconda.com/download/#download)
        - For Mac OS: [https://www.anaconda.com/download/#macos](https://www.anaconda.com/download/#macos)
    + For ubuntu:
        - bash: `pip install jupyter --user`
        - R: `install.packages('IRkernel')`
        - R: `IRkernel::installspec()`
        - bash: `jupyter notebook` -->

- Link to the official R website: [https://www.r-project.org/](https://www.r-project.org/)
- Amazing tutorials: [https://www.r-bloggers.com/](https://www.r-bloggers.com/)
