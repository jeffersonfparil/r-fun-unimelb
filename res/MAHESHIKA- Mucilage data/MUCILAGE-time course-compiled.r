#!/usr/bin/env Rscript

##############################################################
###														   ###
### At which time point 								   ###
### do we minimize within entry variation (error variance) ###
### while maximizing between entry variation? 		       ###
###														   ###
##############################################################

### load libraries
library(agricolae)
library(ggplot2)
library(lme4)
library(readxl)

### import the excel file and convert into a data frame
dat = read_excel("MUCILAGE-time course-compiled.xlsx", sheet=1, na="nan")
dat = as.data.frame(dat)

### assess the data types
str(dat)

### rename columns so that they're easier to write
colnames(dat) = c("ENTRY", "REP", "TIME", "MUCILAGE_RATIO")
str(dat)

### convert the ENTRY and REP categorical factors
dat$ENTRY = as.factor(dat$ENTRY)
dat$REP = as.factor(dat$REP)

### simple boxplots
par(mfrow=c(1,3))
boxplot(dat$MUCILAGE_RATIO ~ dat$ENTRY, xlab="ENTRIES", ylab="MUCILAGE / SEED")
boxplot(dat$MUCILAGE_RATIO ~ dat$TIME, xlab="TIME", ylab="MUCILAGE / SEED")
boxplot(dat$MUCILAGE_RATIO ~ dat$ENTRY + dat$TIME, xlab="ENTRIES x TIME", ylab="MUCILAGE / SEED")

### simple scatter plots
par(mfrow=c(1,1))
plot(x=c(min(dat$TIME), max(dat$TIME)), y=c(min(dat$MUCILAGE_RATIO, na.rm=TRUE), max(dat$MUCILAGE_RATIO, na.rm=TRUE)), type="n", xlab="TIME", ylab="MUCILAGE / SEED")
colours = rainbow(nlevels(dat$ENTRY))
# colours = colors(distinct = TRUE)[sample(nlevels(dat$ENTRY))]
for (i in 1:nlevels(dat$ENTRY)){
	sub = subset(dat, ENTRY==levels(dat$ENTRY)[i])
	points(x=sub$TIME, y=sub$MUCILAGE_RATIO, pch=20, col=colours[i])
	abline(lm(sub$MUCILAGE_RATIO ~ sub$TIME), col=colours[i])
}
legend("topright", legend=levels(dat$ENTRY), fill=colours)

### convert TIME into another categorical factor for the model building and ANOVA
### TECHNICAL QUESTION: To factor or to numeric TIME?--> ask alex
dat$TIME = as.factor(dat$TIME)

### mean comparison per TIME level and extract R^2 adjusted
rm(list=c("HSD.TIME"))
R2.ADJ=c()
for (i in levels(dat$TIME)){
# for (i in unique(dat$TIME)){
	sub = subset(dat, TIME==i)
	mod = lm(MUCILAGE_RATIO ~ ENTRY, data=sub)
	R2.ADJ = c(R2.ADJ, summary(mod)$adj.r.squared)
	HSD = HSD.test(mod, trt="ENTRY")
	HSD.groups = data.frame(ENTRY=rownames(HSD$groups),
							MUCILAGE_RATIO=HSD$groups[,1],
							GROUPING=HSD$groups[,2])
	if(exists("HSD.TIME")==FALSE){
		HSD.TIME = HSD.groups
		colnames(HSD.TIME)[(ncol(HSD.TIME)-1):ncol(HSD.TIME)] = c(paste0("RATIO_t", i), paste0("GROUP_t", i))
	} else {
		HSD.TIME = merge(HSD.TIME, HSD.groups, by="ENTRY", all=TRUE)
		colnames(HSD.TIME)[(ncol(HSD.TIME)-1):ncol(HSD.TIME)] = c(paste0("RATIO_t", i), paste0("GROUP_t", i))
	}
}
R2.ADJ = data.frame(TIME=levels(dat$TIME), R2.ADJ=R2.ADJ)
R2.ADJ
### TIME=120 minimizes the residual variance (maximizes R^2 adjusted)

# HSD.HM = aggregate(dat$MUCILAGE_RATIO ~ dat$ENTRY + dat$TIME, FUN=mean)
# colnames(HSD.HM) = c("ENTRY", "TIME", "MUCILAGE_RATIO")
TIME = rep(levels(dat$TIME), each=nlevels(dat$ENTRY))
# TIME = rep(unique(dat$TIME), each=nlevels(dat$ENTRY))
ENTRY = rep(HSD.TIME$ENTRY, time=nlevels(dat$TIME))
# ENTRY = rep(HSD.TIME$ENTRY, time=length(unique(dat$TIME)))
MEANS = as.vector(as.matrix(HSD.TIME[, seq(from=2, to=ncol(HSD.TIME), by=2)]))
GROUP = as.vector(as.matrix(HSD.TIME[, seq(from=3, to=ncol(HSD.TIME), by=2)]))
HSD.HM = data.frame(ENTRY=ENTRY, TIME=TIME, MUCILAGE_RATIO=MEANS, GROUP=GROUP)

p = ggplot(data=HSD.HM, aes(x=reorder(TIME, sort(as.numeric(as.character(TIME)))), y=ENTRY, fill=MUCILAGE_RATIO)) + 
	geom_tile() +
	scale_fill_gradient2(low="blue", high="red", mid="white", 
		midpoint=median(HSD.HM$MUCILAGE_RATIO, na.rm=TRUE), 
		limit = c(min(HSD.HM$MUCILAGE_RATIO, na.rm=TRUE),max(HSD.HM$MUCILAGE_RATIO, na.rm=TRUE)),
		space = "Lab", 
		name="Mucilage/Seed Area\nRatio") +
	xlab("TIME") + ylab("ENTRY") +
	theme_gray()
p

### hierarchical model nesting ENTRY within TIME (random ENTRY intecepts and slopes through TIME)
### to find ENTRY rank at the intercept and which has slopes significantly different from zero at in which direction: sloping up or down?
mod_lmer0 = lmer(MUCILAGE_RATIO ~ (TIME|ENTRY), data=dat)
mod_lmer1 = lmer(MUCILAGE_RATIO ~ (0+TIME|ENTRY), data=dat)
mod_lmer2 = lmer(MUCILAGE_RATIO ~ TIME + (1|ENTRY), data=dat)
anova(mod_lmer0, mod_lmer1, mod_lmer2)
summary(mod_lmer2)
ranef(mod_lmer2)
fixef(mod_lmer2)

### using the 2nd model to replot the heatmap but with BLUPs!
BLUPS = ranef(mod_lmer1)$ENTRY + fixef(mod_lmer1)
TIME = rep(gsub("TIME", "", colnames(BLUPS)), each=nrow(BLUPS))
ENTRY = rep(rownames(BLUPS), times=ncol(BLUPS))
BLUPS = as.vector(as.matrix(BLUPS))
BLUPS.HM = data.frame(ENTRY=ENTRY, TIME=TIME, BLUPS=BLUPS)

p2 = ggplot(data=BLUPS.HM, aes(x=reorder(TIME, sort(as.numeric(as.character(TIME)))), y=ENTRY, fill=BLUPS)) + 
	geom_tile() +
	scale_fill_gradient2(low="blue", high="red", mid="white", 
		midpoint=median(BLUPS.HM$BLUPS), 
		limit = c(min(BLUPS.HM$BLUPS),max(BLUPS.HM$BLUPS)),
		space = "Lab", 
		name="Mucilage/Seed Area\nRatio BLUPs") +
	xlab("TIME") + ylab("ENTRY") +
	theme_gray()
p2
ggsave("MUCILAGE-time course-compiled BLUPs heatmap.jpg", plot=p2, height=20, width=30, units="cm", dpi=300)

BLUPS.TIME.VAR = aggregate(BLUPS.HM$BLUPS ~ BLUPS.HM$TIME, FUN=var)
BLUPS.TIME.VAR = BLUPS.TIME.VAR[order(BLUPS.TIME.VAR[,2], decreasing=TRUE), ]
colnames(BLUPS.TIME.VAR) = c("TIME", "BETWEEN_ENTRIES_VARIANCE")

### merge the BLUPS between entries variance and the individual time point model R square adjusted 
OUT = merge(R2.ADJ, BLUPS.TIME.VAR, by="TIME")
write.csv(OUT, file="MUCILAGE-time course-compiled R2adj and between entries variance.csv", row.names=FALSE)

### conclusion
### 120 hours of staining minimizes the variation within entries (minimum error variance)
### while maximizing variation between entries (maximum power to discriminate between genotype)
